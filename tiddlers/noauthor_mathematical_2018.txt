@misc{noauthor_mathematical_2018,
	title = {Mathematical optimization},
	copyright = {Creative Commons Attribution-ShareAlike License},
	url = {https://en.wikipedia.org/w/index.php?title=Mathematical_optimization&oldid=858031363},
	abstract = {In mathematics, computer science and operations research, mathematical optimization or mathematical programming, alternatively spelled optimisation, is the selection of a best element (with regard to some criterion) from some set of available alternatives.In the simplest case, an optimization problem consists of maximizing or minimizing a real function by systematically choosing input values from within an allowed set and computing the value of the function. The generalization of optimization theory and techniques to other formulations constitutes a large area of applied mathematics. More generally, optimization includes finding "best available" values of some objective function given a defined domain (or input), including a variety of different types of objective functions and different types of domains.},
	language = {en},
	urldate = {2018-09-09},
	journal = {Wikipedia},
	month = sep,
	year = {2018},
	note = {00047 
Page Version ID: 858031363},
	file = {Snapshot:/home/akira/Zotero/storage/J3AWXIIH/index.html:text/html}
}