@misc{noauthor_file:automata_nodate,
	title = {File:{Automata} theory.svg},
	copyright = {Creative Commons Attribution-ShareAlike License},
	shorttitle = {File},
	url = {https://en.wikipedia.org/wiki/File:Automata_theory.svg},
	language = {en},
	urldate = {2018-09-08},
	journal = {Wikipedia},
	note = {00000},
	file = {Snapshot:/home/akira/Zotero/storage/3QVKUP87/FileAutomata_theory.html:text/html}
}