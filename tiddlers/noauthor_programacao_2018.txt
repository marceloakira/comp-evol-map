@misc{noauthor_programacao_2018,
	title = {Programação linear},
	copyright = {Creative Commons Attribution-ShareAlike License},
	url = {https://pt.wikipedia.org/w/index.php?title=Programa%C3%A7%C3%A3o_linear&oldid=51742862},
	abstract = {Em matemática, problemas de Programação Linear (PL) são problemas de optimização nos quais a função objetivo e as restrições são todas lineares.
Programação Linear é uma importante área da optimização por várias razões. Muitos problemas práticos em pesquisa operacional podem ser expressos como problemas de programação linear. Certos casos especiais de programação linear, tais como problemas de network flow e problemas de multicommodity flow são considerados importantes o suficiente para que se tenha gerado muita pesquisa em algoritmos especializados para suas soluções. Vários algoritmos para outros tipos de problemas de optimização funcionam resolvendo problemas de PL como sub-problemas.  Historicamente, ideias da programação linear inspiraram muitos dos conceitos centrais de teoria da optimização, tais como dualidade, decomposição, e a importância da convexidade e suas generalizações.},
	language = {pt},
	urldate = {2018-09-10},
	journal = {Wikipédia, a enciclopédia livre},
	month = apr,
	year = {2018},
	note = {00007 
Page Version ID: 51742862},
	file = {Snapshot:/home/akira/Zotero/storage/A9PS85KG/index.html:text/html}
}