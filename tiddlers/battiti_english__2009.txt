@misc{battiti_english:_2009,
	title = {English: {Local} search creates attraction basins in the configuration space.},
	copyright = {Permission is granted to copy, distribute and/or modify this document under the terms of the GNU Free Documentation License, Version 1.2 or any later version published by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts. A copy of the license is included in the section entitled GNU Free Documentation License.http://www.gnu.org/copyleft/fdl.htmlGFDLGNU Free Documentation Licensetruetrue},
	shorttitle = {English},
	url = {https://commons.wikimedia.org/wiki/File:Local_search_attraction_basins.png},
	urldate = {2018-09-10},
	author = {Battiti, Roberto},
	month = jan,
	year = {2009},
	note = {00000},
	file = {Wikimedia Snapshot:/home/akira/Zotero/storage/6878HLEH/FileLocal_search_attraction_basins.html:text/html}
}