
@misc{wikipedia_genetic_programming_2018,
	title = {Genetic programming},
	copyright = {Creative Commons Attribution-ShareAlike License},
	url = {https://en.wikipedia.org/w/index.php?title=Genetic_programming&oldid=846518953},
	abstract = {In artificial intelligence, genetic programming (GP) is a technique whereby computer programs are encoded as a set of genes that are then modified (evolved) using an evolutionary algorithm (often a genetic algorithm, "GA") – it is an application of (for example) genetic algorithms where the space of solutions consists of computer programs. The results are computer programs that are able to perform well in a predefined task. The methods used to encode a computer program in an artificial chromosome and to evaluate its fitness with respect to the predefined task are central in the GP technique and still the subject of active research.},
	language = {en},
	urldate = {2018-09-17},
	journal = {Wikipedia},
	month = jun,
	year = {2018},
	note = {00006 
Page Version ID: 846518953},
	file = {Snapshot:/home/akira/Zotero/storage/5CN6YKKY/index.html:text/html}
}