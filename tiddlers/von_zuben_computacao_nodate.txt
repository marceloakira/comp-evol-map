@misc{von_zuben_computacao_nodate,
	title = {Computação evolutiva: uma abordagem pragmática},
	url = {https://www.ic.unicamp.br/~rocha/teaching/2011s2/mc906/aulas/computacao-evolutiva-uma-abordagem-pragmatica.pdf},
	urldate = {2018-08-29},
	author = {Von Zuben, Fernando J}
}