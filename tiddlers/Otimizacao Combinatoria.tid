created: 20180909175838677
modified: 20180909183943445
tags: conceito
title: Otimização Combinatória
tmap.edges: {"b63b0410-a1f8-4186-8753-92b8af6cc07d":{"to":"4d2abb17-8548-46f8-937a-c75bf73d1309","type":"é um(a)"}}
tmap.id: 83ac26c5-71de-462c-a111-f6f871857de0

! Definições

Formalmente um problema de otimização combinatória $$ A $$  é uma quádrupla $$ (I, f, m, g) $$, onde:

* $$I$$ é um conjunto de instâncias;
* dada uma instância $$ x \in I $$, $$ f(x) $$ é o conjunto de soluções viáveis;
* dada um instância $$ x $$ e uma solução viável $$ y $$ de $$ x $$,  $$ m(x, y) $$ denota a medida de $$ y $$, o qual é geralmente um número real positivo.
* $$ g $$ é a função objetivo, e é geralmente $$ min $$ ou $$ max $$

O objetivo é então encontrar para uma instância $$ x $$ uma ''solução ótima'', ou seja, uma solução viável $$ y $$ com:
 
$$
m(x, y) = g \{ m(x, y') \mid y' \in f(x) \} .
$$

Fonte: [[noauthor_optimization_nodate]]