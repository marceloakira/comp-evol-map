
@misc{wikipedia_genetic_operator_2015,
	title = {Genetic operator},
	copyright = {Creative Commons Attribution-ShareAlike License},
	url = {https://en.wikipedia.org/w/index.php?title=Genetic_operator&oldid=677152013},
	abstract = {A genetic operator is an operator used in genetic algorithms to guide the algorithm towards a solution to a given problem. There are three main types of operators (mutation, crossover and selection), which must work in conjunction with one another in order for the algorithm to be successful. Genetic operators are used to create and maintain genetic diversity (mutation operator), combine existing solutions (also known as chromosomes) into new solutions (crossover) and select between solutions (selection). In his book discussing the use of genetic programming for the optimization of complex problems, computer scientist John Koza has also identified an 'inversion' or 'permutation' operator; however, the effectiveness of this operator has never been conclusively demonstrated and this operator is rarely discussed.Mutation (or mutation-like) operators are said to be unary operators, as they only operate on one chromosome at a time. In contrast, crossover operators are said to be binary operators, as they operate on two chromosomes at a time, combining two existing chromosomes into one new chromosome.},
	language = {en},
	urldate = {2018-09-17},
	journal = {Wikipedia},
	month = aug,
	year = {2015},
	note = {00006 
Page Version ID: 677152013},
	file = {Snapshot:/home/akira/Zotero/storage/BG5V7G5T/index.html:text/html}
}