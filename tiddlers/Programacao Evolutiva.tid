created: 20180908052706909
modified: 20181030194040175
tags: conceito
title: Programação Evolutiva
tmap.edges: {"2c151c05-c490-43b0-ad0a-d8130bc57575":{"to":"9eb396ca-7a56-434e-be1c-fb55fe1fdbd6","type":"é um(a)"},"d0517bb7-914e-40fe-a64a-562d04a7a9d5":{"to":"91d60f2d-ecff-4184-9454-cf1a84c84d78","type":"originalmente utilizou"},"a2293228-a235-4680-b4a3-fa108f6ca6e9":{"to":"56497d55-c7a8-4ad5-88b1-718c0d7f8de1","type":"semelhante"},"c29de366-c968-4725-a3c1-9fdcb0f7dbb4":{"to":"7162f7a7-1351-47e3-b76e-e964eedfe6d8","type":"não utiliza"},"285f3bd7-bfb5-4c9a-be9e-411322558aba":{"to":"019fe989-d9d2-45a2-a9c7-424b7da1181c","type":"utiliza"},"96b82b52-e6dc-41ba-8d7b-74171ecc6a72":{"to":"fd1ba6d7-788a-4a33-b481-cb940ab79f79","type":"utiliza"},"180a9bfc-b468-483b-a434-34efb107491c":{"to":"58d3367d-8a42-490e-b320-7c9f8d1ad4c4","type":"utiliza"}}
tmap.id: 7678ba7e-8716-40ca-9af1-d998fab2c8a1
type: text/vnd.tiddlywiki

!! Mapas
* Específico: [[Mapa: Programação Evolucionária]]
* Nível acima: [[Mapa da Computação Evolutiva]]

!! Sub-classe de:
* [[Algoritmo Evolucionário]]

!! Definições

!!! [[fogel__d._b_whats_1994]]:
> EVOLUTIONARY PROGRAMMING, originally conceived by Lawrence J. Fogel in 1960, is a stochastic OPTIMIZATION strategy similar  to GENETIC ALGORITHMs, but instead places emphasis on the  behavioral linkage between PARENTS and their OFFSPRING,  rather than seeking to emulate specific GENETIC OPERATORS as  observed in nature.  EVOLUTIONARY PROGRAMMING is similar to  EVOLUTION STRATEGIES, although the two approaches developed independently (see below).

Tradução:
Programação Evolutiva (PE), originalmente concebida por Lawrence J. Fogel em 1960, é uma estratégia de OTIMIZAÇÃO estocástica similar a ALGORITMOS GENÉTICOS (AG), no entanto enfatiza a ligação comportamental entre PAIS e seus DESCENDENTES,  em vez de emular OPERADORES GENÉTICOS como observado na natureza. PROGRAMAÇÃO EVOLUTIVA é similar a ESTRATÉGIAS EVOLUTIVAS (EE), apesar das duas abordagens terem sido desenvolvidas independentemente.

!!! [[fernando_von_zuben_programacao_1999]]:
> - L. FOGEL et al. (1966) propuseram a programação evolutiva como  técnica de simulação da evolução para desenvolver uma forma alternativa de inteligência artificial. O comportamento inteligente era visto como requerendo as seguintes habilidades: - ''Predizer um determinado ambiente e responder apropriadamente a este ambiente baseado na predição feita.'' 
> O ambiente foi considerado de forma genérica como sendo ''descrito por uma sequência de símbolos tomados a partir de um alfabeto finito''. 
> O problema evolutivo foi então definido como sendo a evolução de um algoritmo que  pudesse  ''operar  na  sequência  de  símbolos  até  então  observada  de  tal  forma  a produzir  um  símbolo  de  saída  que  maximizaria  o  desempenho  do  algoritmo  em relação  ao  próximo  símbolo  a  ser  apresentado,  dada  uma  função  de  custo  bem definida.''
> ''Máquinas de estado finito'' (FSM) são uma representação apropriada a estes propósitos.

!! Características:
!!! [[fernando_von_zuben_programacao_1999]]:
* representado por [[Máquinas de Estado Finito - FSM|Máquinas de Estado Finito]]
* não utiliza [[recombinação (crossover)|Recombinação]]
* adota abordagem //top-down//: utilizar a evolução para criar "organismos" cada vez mais "inteligentes" ao longo do tempo.  

!! Pseudo-código:
[[fogel__d._b_whats_1994]]:

```
PSEUDO CODE
Algorithm EP is
// start with an initial time
t := 0;
// initialize a usually random population of individuals
initpopulation P(t);
// evaluate fitness of all initial individuals of population
evaluate P(t);
// test for termination criterion (time, fitness, etc.)
while not done do
	// perturb the whole population stochastically
	P'(t) := mutate P(t);
	// evaluate it's new fitness
	evaluate P'(t);
	// stochastically select the survivors from actual fitness
	P(t+1) := survive P(t),P'(t);
	// increase the time counter
	t := t + 1;
od
end EP.
```

!! Exemplos de experimentos e aplicações
[[fernando_von_zuben_programacao_1999]]:

* Predição se um número binário é primo ou não (Fogel et al., 1996):
** entrada: número representado em base binária
** saída: 1 (primo) ou 0 (não-primo)
** resultado esperado: classificação correta de número primo 
** resultados obtidos: apresentaram evidências de aprendizagem sem nenhum conhecimento prévio explícito de um número primo

* Evolução do olho humano (RECHENBERG , 1994):
** entrada: lente ótica com espessura única ($$ d_k = c, (k=1,...,n) $$), sendo $$ d $$ a espessura da lente, $$ c $$ uma constante e $$ k $$ o número de pontos de 1 a n. 
** saída:  distância $$ q_k, k=1,...,n $$ do ponto atingido por um raio de luz a um ponto central P em um anteparo plano (fazendo o papel de retina) 
** resultado esperado: evolução para o formato de um olho humano (lente ocular)
** resultado obtido: lente ocular

* Outros experimentos
** Evolução de Um Conjunto Articulado em um Túnel de Vento
** Projeto Evolutivo de Uma Ponte Articulada 

!! Código fonte em Ruby (noauthor_evolutionary_nodate):

```
# Evolutionary Programming algorithm in the Ruby Programming Language

# The Clever Algorithms Project: http://www.CleverAlgorithms.com
# (c) Copyright 2010 Jason Brownlee. Some Rights Reserved. 
# This work is licensed under a Creative Commons Attribution-Noncommercial-Share Alike 2.5 Australia License.

def objective_function(vector)
    return vector.inject(0.0) {|sum, x| sum +  (x ** 2.0)}
  end
  
  def random_vector(minmax)
    return Array.new(minmax.size) do |i|      
      minmax[i][0] + ((minmax[i][1] - minmax[i][0]) * rand())
    end
  end
  
  def random_gaussian(mean=0.0, stdev=1.0)
    u1 = u2 = w = 0
    begin
      u1 = 2 * rand() - 1
      u2 = 2 * rand() - 1
      w = u1 * u1 + u2 * u2
    end while w >= 1
    w = Math.sqrt((-2.0 * Math.log(w)) / w)
    return mean + (u2 * w) * stdev
  end
  
  def mutate(candidate, search_space)
    # puts "candidate: #{candidate.class}"
    child = {:vector=>[], :strategy=>[]}
    candidate[:vector].each_with_index do |v_old, i|
      s_old = candidate[:strategy][i]
      v = v_old + s_old * random_gaussian()
      v = search_space[i][0] if v < search_space[i][0]
      v = search_space[i][1] if v > search_space[i][1]
      child[:vector] << v
      child[:strategy] << s_old + random_gaussian() * s_old.abs**0.5
    end
    return child
  end
  
  def tournament(candidate, population, bout_size)
    candidate[:wins] = 0
    bout_size.times do |i|
      other = population[rand(population.size)]
      candidate[:wins] += 1 if candidate[:fitness] < other[:fitness]
    end  
  end
  
  def init_population(minmax, pop_size)
    strategy = Array.new(minmax.size) do |i| 
      [0,  (minmax[i][1]-minmax[i][0]) * 0.05]
    end
    pop = Array.new(pop_size, {})
    pop.each_index do |i|
      pop[i][:vector] = random_vector(minmax)
      pop[i][:strategy] = random_vector(strategy)
    end
    pop.each{|c| c[:fitness] = objective_function(c[:vector])}
    return pop
  end
  
  def search(max_gens, search_space, pop_size, bout_size)
    population = init_population(search_space, pop_size)
    population.each{|c| c[:fitness] = objective_function(c[:vector])}
    best = population.sort{|x,y| x[:fitness] <=> y[:fitness]}.first  
    max_gens.times do |gen|
      children = Array.new(pop_size) {|i| mutate(population[i], search_space)}
      children.each{|c| c[:fitness] = objective_function(c[:vector])}
      children.sort!{|x,y| x[:fitness] <=> y[:fitness]}
      best = children.first if children.first[:fitness] < best[:fitness]
      union = children+population
      union.each{|c| tournament(c, union, bout_size)}
      union.sort!{|x,y| y[:wins] <=> x[:wins]}
      population = union.first(pop_size)
      puts " > gen #{gen}, fitness=#{best[:fitness]}"
    end  
    return best
  end
  
  if __FILE__ == $0
    # problem configuration
    problem_size = 2
    search_space = Array.new(problem_size) {|i| [-5, +5]}
    # algorithm configuration
    max_gens = 200
    pop_size = 100
    bout_size = 5
    # execute the algorithm
    best = search(max_gens, search_space, pop_size, bout_size)
    puts "done! Solution: f=#{best[:fitness]}, s=#{best[:vector].inspect}"
  end
```

* Transcrição de algumas funções do código-fonte acima para python:

```
# objective function
def objective_function(vector):
    s = 0
    for e in vector:
        # sphere function. more functions: https://en.wikipedia.org/wiki/Test_functions_for_optimization
        s = s + e**2
    return s

# v = [2, 3, 4]
# print(objective_function(v))
# 29 (expected result)

import random
def random_vector(minmax):
    rv = []
    for i in range(0, len(minmax)):
        rand = minmax[i][0] + ((minmax[i][1] - minmax[i][0]) * random.random())
        rv.append(rand)
    return rv
# search_space = [[-5, 5],[-5,5]]
# rv = random_vector(search_space)
# print(rv)

import math
def random_gaussian(mean=0.0, stdev=1.0):
    u1 = u2 = w = 0
    while True:
        u1 = 2 * random.random() - 1
        u2 = 2 * random.random() - 1
        w = u1 * u1 + u2 * u2
        if w < 1:
            break
    w = math.sqrt((-2.0 * math.log(w)))
    return mean + (u2 * w) * stdev

# rg = random_gaussian()
# print(rg)


``` 